import React, { Component } from "react";  
import ReactDOM from "react-dom" //the way to connect app with index.html
import './styles/style.css'
import JSON from './db.json'

import Header from "./components/header";
import NewsList from "./components/news_list";
import Footer from "./components/footer";
import Life from "./components/lifecycle";

class App extends Component {  //firs letter must be in uppercase
    
    state = {
        news: JSON,
        filtered: JSON,
        footerText: 'I am a happy footer',
        active: true
    }

    getKeywords = (event) => {
        let keywords = event.target.value;
        let filtered = this.state.news.filter((item) => {
            return item.title.indexOf(keywords) > -1
        })

        this.setState({filtered})
    }



    render(){
        const {news, footerText, filtered, active} = this.state    
        return (  //React.createElement('h1', {className: "title"}, React.createElement('div')) /-this way suks    
            <>
            <Header 
                keywords={this.getKeywords}
            />
            
            {/* <NewsList news={filtered}>
                <br />
                <h1>I am a children</h1>
                <h2>I am a children</h2>
            </NewsList> */}

            {active?
                <Life />
            : null}

            <button
                onClick={() => this.setState({active:!active})}
            >Action</button>

            <Footer footerText={footerText}/>
            </>
        ) 
    } 
} 

ReactDOM.render(<App/>, document.getElementById("root"))