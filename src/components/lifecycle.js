import React, { Component } from "react";

export default class Life extends Component {
    constructor(props){
        super(props);
        
        this.state = {name: 'Roman'}
        console.log("1 - constructor")
    }

    static getDerivedStateFromProps(props, state){
        console.log("2 - getDerivedStateFromProps")

        if(state.name === "Ran"){
            return {
                name: 'Petro'
            }
        }
        return null
    }

    componentDidMount(){
        console.log("4 - componentDidMount")
    }

    componentWillUnmount(){
        console.log("5 - componentWillUnmount")
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('x-shouldComponentUpdate')
        if(nextState.name === "Ron"){
            return false
        }
       
        return true
    }

    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('x-componentDidUpdate')
        console.log(snapshot)
    }

    getSnapshotBeforeUpdate(prevProps, prevState, snapshot){
        let age = 250;
        
        return age
    }

    
    render(){
        console.log("3 - render")
        return <div>
            <div>{this.state.name}</div>
            <div onClick={() => this.setState({name:'Ran'})}>
                change name
            </div>
        </div>
    }
}