import React from "react";
import NewsItem from "./news_list_item";


const NewsList = (props) => {

    const news = props.news.map((item)=> (
        <NewsItem key={item.id} item={item}/>
    ))

    return(
        <>
             { news }
             {props.children}
        </>
    )
}

export default NewsList;